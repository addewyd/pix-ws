﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.WebSockets;

namespace Activities.TL.Reporter2
{
    public static class AsyncHelper
    {
        private static readonly TaskFactory _taskFactory = new
            TaskFactory(CancellationToken.None,
                        TaskCreationOptions.None,
                        TaskContinuationOptions.None,
                        TaskScheduler.Default);

        public static TResult RunSync<TResult>(Func<Task<TResult>> func)
            => _taskFactory
                .StartNew(func)
                .Unwrap()
                .GetAwaiter()
                .GetResult();

        public static void RunSync(Func<Task> func)
            => _taskFactory
                .StartNew(func)
                .Unwrap()
                .GetAwaiter()
                .GetResult();
    }

    // ...............................................................................

    public static class HelperExtensions
    {
        public static Task GetContextAsync(this HttpListener listener)
        {
            return Task.Factory.FromAsync<HttpListenerContext>(listener.BeginGetContext, listener.EndGetContext, TaskCreationOptions.None);
        }

        public static Task SendStringAsync(this WebSocket ws, string toSend, CancellationToken ctoken)
        {
            var encoded = System.Text.Encoding.UTF8.GetBytes(toSend);
            var buffer = new ArraySegment<Byte>(encoded, 0, encoded.Length);
            return ws.SendAsync(buffer, WebSocketMessageType.Text, true, ctoken);
        }
    }

    // .........................................................................................

    public class Logger
    {
        private readonly static object _logLock = new object();
        private string filename;

        public string FileName { 
            get =>  filename; 
            set {
                string logPath = System.Environment.CurrentDirectory + @"\logs";
                filename = logPath + @"\" + value;
            } }
        bool silent;
        public Logger(string f, bool on = true)
        {
            silent = !on;
            string logPath = System.Environment.CurrentDirectory + @"\logs";           
            filename = logPath + @"\" + f;
        }

        // .........................................................................................

        public void log(string msg)
        {
            if (silent) return;
            try
            {
                lock (_logLock)
                {
                    var w = System.IO.File.AppendText(filename);
                    try
                    {
                        w.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} {msg}");
                    }
                    finally
                    {
                        w.Close();
                    }
                }
            }
            catch (Exception ex) { }
        }
    }
}
