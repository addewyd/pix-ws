﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.WebSockets;
using BR.Core;
using BR.Core.Attributes;
using System.Threading;
using System.Threading.Tasks;


namespace Activities.TL.Reporter2
{
    // .............................................................................................................

    [ScreenName("WSClient")]
    [Description("WSClient")]
    [Path("TL.Net")]
    [Representation("WSClient")]
    [OptionList("Connect", "Send/Receive", "Close", "State")]

    class WSClient : Activity
    {
        static ClientWebSocket webSocket = null;
        int receiveChunkSize = 8192;
        static string OurID;
        static Logger logger;

        public void Connect(string uri)
        {
            logger = new Logger("wsclients", WriteLog);
            logger.log($"Connect {BotName}");
            if(!(webSocket is null))
            {
                logger.log($"Close {BotName} before Connect");
                return;
            }
            try
            {
                webSocket = new ClientWebSocket();
                
                AsyncHelper.RunSync(() => { return webSocket.ConnectAsync(new Uri(uri), CancellationToken.None); });
                logger.log("connect ws 2 " + webSocket.State.ToString());

                var data = new Dictionary<string, string>()
                {
                    {"type", "connect"},
                    {"name", BotName},
                    {"clientType", "pix"}
                };
                string s = System.Text.Json.JsonSerializer.Serialize(data);
                Send(s);

                logger.log($"Connect ok {BotName} " + Response);
            }
            catch (Exception ex)
            {
                logger.log($"Exception: {ex.Message}");
            }
        }

        // ....................

        private void Send(string data)
        {
            try
            {
                logger.log("Send " + data);

                AsyncHelper.RunSync(() => { return webSocket.SendStringAsync(data,  CancellationToken.None); });
                string res = Receive();
                var r = System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string>>(res);
                res = ProcessResponse(res);
                Response = res;
            } catch(Exception ex)
            {
                logger.log($"Send Exception: {ex.Message}");
                throw ex;
            }
        }

        // ..........................................................................................

        void SendData()
        {
            var msg = new Dictionary<string, string>();
            msg["type"] = "message";
            msg["id"] = OurID;
            msg["data"] = Data;
            msg["clientType"] = "pix";
            var js = System.Text.Json.JsonSerializer.Serialize(msg);
            Send(js);
        }

        // ..........................................................................................

        string ProcessResponse(string res)
        {
            string r = res;

            var d = System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string>>(res);

            if(d.ContainsKey("yourId"))
            {
                OurID = d["yourId"];
                logger.log($"Continue in {OurID}.log");
                logger.FileName = OurID + ".log";
                logger.log($"Bot name {BotName}");
                logger.log($"Our id {OurID}");
            }

            if (!d.ContainsKey("echo"))
            {
                logger.log($"Response processed {res}");
            }
            return res;

        }

        // ..........................................................................................

        private string Receive()
        //private async Task<string> Receive()
        {
            logger.log("Receive");

            ArraySegment<Byte> buffer = new ArraySegment<byte>(new byte[receiveChunkSize]);

            WebSocketReceiveResult result = null;
            try
            {
                using (var ms = new System.IO.MemoryStream())
                {
                    do
                    {
                        result = AsyncHelper.RunSync(() => { return webSocket.ReceiveAsync(buffer, CancellationToken.None); });
                        ms.Write(buffer.Array, buffer.Offset, result.Count);
                    }
                    while (!result.EndOfMessage);

                    ms.Seek(0, System.IO.SeekOrigin.Begin);

                    using (var reader = new System.IO.StreamReader(ms, System.Text.Encoding.UTF8))
                        return reader.ReadToEnd();
                }
            } catch(Exception ex)
            {
                logger.log($"Receive Exception: {ex.Message}");
                throw ex;
            }
        }

        // ................................................................................

        public void Close()
        {
            try
            {
                AsyncHelper.RunSync(() => { return webSocket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "Close", CancellationToken.None); });
                logger.log("Close");
                if (webSocket != null)
                {
                    webSocket.Dispose();
                    webSocket = null;
                }
            }
            catch (Exception ex)
            {
                logger.log($"Close Exception: {ex.Message}");
                throw ex;
            }
        }
        // ................................................................................

        public void ShowState()
        {
            if (webSocket is null)
            {
                State = "null";
            }
            else
            {
                State = webSocket.State.ToString();
            }
        }

        // ................................................................................

        [ScreenName("Uri")]
        [Description("Uri")]
        [IsRequired]
        [Options(0)]
        public string Uri { get; set; } = "ws://localhost:3010/pix/";

        [ScreenName("Name")]
        [Description("BotName")]
        [IsRequired]
        [Options(0)]
        public string BotName { get; set; } = "Pix Robot";

        [ScreenName("Logger")]
        [Description("Logger")]
        [IsRequired]
        [Options(0)]
        public bool WriteLog { get; set; } = true;

        [ScreenName("Data")]
        [Description("Data")]
        [IsRequired]
        [Options(1)]
        public string Data { get; set; }

        [ScreenName("Response")]
        [Description("Response")]
        [IsRequired]
        [Options(1)]
        [IsOut]
        public string Response { get; set; }

        [ScreenName("State")]
        [Description("Statu")]
        [IsRequired]
        [Options(3)]
        [IsOut]
        public string State { get; set; }

        override public void Execute(int? opt)
        {
            switch (opt)
            {
                case 0:
                    Connect(Uri);
                    break;
                case 1:
                    SendData();
                    break;
                case 2:
                    Close();
                    break;
                case 3:
                    ShowState();
                    break;
                default: break;
            }
        }
    }
}

