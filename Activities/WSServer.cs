﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.WebSockets;
using BR.Core;
using BR.Core.Attributes;
using System.Threading;
using System.Threading.Tasks;

namespace Activities.TL.Reporter2
{    
    class WSId
    {
        public WebSocket ws { get; set; }
        public string clientType { get; set; }
        public string clientName { get; set; }
    }

    class Server
    {
        private static object _qLock = new object();
        HttpListener listener;

        Logger logger;
        bool debug = false   ;

        // set of sockets connected to clients
        Dictionary<string, WSId> SocketSet = new Dictionary<string, WSId>();

        // Collection of queues caontaining messages form "web" to "pix"
        Dictionary<string, Queue<Dictionary<string, string>>> ClientQueues = new Dictionary<string, Queue<Dictionary<string, string>>>();

        CancellationTokenSource cancelTokenSource;// = new CancellationTokenSource();
        CancellationToken ctoken;

        public Server(bool d)
        {
            debug = d;
            logger = new Logger("wsserver", true);
            logger.log("new Server");
            cancelTokenSource = new CancellationTokenSource();
            ctoken = cancelTokenSource.Token;
            ctoken.Register(() =>
            {
                var b = this.listener.IsListening;
                var ct = this.ctoken.GetType();
                // L2
                logger.log($"ctoken action; listening {b}  {ct}");
            });
            
        }

        // .............................................................................

        public async void Start(string listenerPrefix)
        {
            listener = new HttpListener();
            logger.log("new Listener");
            var lps = listenerPrefix.Split(';');
            try
            {
                try
                {
                    foreach (var lp in lps)
                    {
                        if (lp.StartsWith("h", StringComparison.OrdinalIgnoreCase))
                        {
                            logger.log($"adding {lp}");
                            listener.Prefixes.Add(lp);
                            logger.log($"added {lp}");
                        }
                    }
                    listener.Start();
                    logger.log("Listening...");
                }
                catch (Exception ex)
                {
                    logger.log("At start " + ex.Message + "\n" + ex.StackTrace);
                    throw ex;
                }
                while (true)
                {
                    HttpListenerContext listenerContext = await listener.GetContextAsync();  // here

                    if (listenerContext.Request.IsWebSocketRequest)
                    {
                        ProcessRequest(listenerContext);
                    }
                    else
                    {
                        listenerContext.Response.StatusCode = 400;
                        listenerContext.Response.Close();
                    }
                }
            }
            catch (Exception ex)
            {    
                // L5  EndGetContext
                logger.log("Catched " + ex.Message + "\n" + ex.StackTrace);
                
            }
            finally
            {
                if (listener.IsListening)
                {
                        listener.Stop();
                        listener.Prefixes.Clear();
                        listener.Close();
                        logger.log("finally STOP OK");
                } else
                {
                    logger.log("finally STOP OK, listener closed");
                }
            }
        }

        // .....................................................................................

        private async void ProcessRequest(HttpListenerContext listenerContext)
        {
            WebSocketContext webSocketContext = null;
            try
            {                
                webSocketContext = await listenerContext.AcceptWebSocketAsync(subProtocol: null);
            }
            catch (Exception e)
            {
                listenerContext.Response.StatusCode = 500;
                listenerContext.Response.Close();
                logger.log($"Exception 2: {e}");
                return;
            }

            WebSocket webSocket = webSocketContext.WebSocket;

            try
            {
                byte[] receiveBuffer = new byte[1024];

                while (webSocket.State == WebSocketState.Open)
                {
                    WebSocketReceiveResult receiveResult = await webSocket.ReceiveAsync(new ArraySegment<byte>(receiveBuffer), ctoken);
                    logger.log("Receive " + receiveResult.MessageType.ToString());
                    if (receiveResult.MessageType == WebSocketMessageType.Close)
                    {
                        // Send notification to web clients
                        ProcessStop(webSocket);
                        await webSocket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", ctoken);
                    }
                    else if (receiveResult.MessageType == WebSocketMessageType.Text)
                    {
                        var s = new ArraySegment<byte>(receiveBuffer, 0, receiveResult.Count);                        
                        string k = System.Text.Encoding.UTF8.GetString(receiveBuffer, 0, receiveResult.Count);

                        logger.log("Got: " + k);

                        try
                        {
                            ProcessMessage(k, webSocket);
                        } catch(Exception ex)
                        {
                            logger.log("Exc in ProcessMessage " + ex.Message + "\n" + ex.StackTrace);
                        }
                                                
                    }
                    else
                    {
                        throw new Exception($"Incorrect message type {receiveResult.MessageType.ToString()}");
                    }
                }
            }
            catch (System.Threading.Tasks.TaskCanceledException ex)
            {
                //L2
                logger.log($"Cancel Exception 1-0 " + ex.Message + "\n" + ex.StackTrace);
            }
            catch (System.Net.WebSockets.WebSocketException ex)
            {
                logger.log($"WS Exception 1-1 " + ex.Message + "\n" + ex.StackTrace);
            }
            catch (Exception ex)
            {
                logger.log($"Exception 1-2: {ex}" + "\n" + ex.StackTrace);
            }
            finally
            {
                if (webSocket != null)
                {
                    // Clear websocket associated data
                    lock (_qLock)
                    {
                        foreach(var k in SocketSet.Keys)
                        {
                            var wi = SocketSet[k];
                            if(wi.ws.Equals(webSocket)) {
                                // L3 C;ear webLocket data
                                logger.log("Eq ws delete " + k);
                                    SocketSet.Remove(k);
                                    ClientQueues.Remove(k);
                                break;
                            }
                        }
                    }
                    webSocket.Dispose();
                    logger.log("ws disposed");
                }
            }
        }

        // ............................................................................................

        // send close notifications to connected webpage sokets
        async void ProcessStop(WebSocket ws) {
            
            foreach(var id in SocketSet.Keys) { 
                var w = SocketSet[id].ws;
                if (ws == w)
                {
                    // got current socket id
                    logger.log($"Found id to close {id}");
                    foreach (var key in SocketSet.Keys)
                    {
                        var clientType = SocketSet[key].clientType;
                        var ww = SocketSet[key].ws;

                        // Send close notification to web pages
                        if (clientType == "web")
                        {
                            // this message allows  eg to remove client info block from webpage
                            Dictionary<String, string> rd = new Dictionary<string, string>()
                            {
                                {"id",  id},
                                {"type", "close"}
                            };
                            string ret = System.Text.Json.JsonSerializer.Serialize(rd);

                            await ww.SendStringAsync(ret, ctoken);
                            logger.log($"close to web sent");
                        }
                    }
                    break;
                }
            }
        }
        
        // ..............................................................

        async void ProcessMessage(string message, WebSocket ws)
        {
            var msg = System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string>>(message);
            if (msg.ContainsKey("type"))
            {
                // on the connect query we create an unique id and send it to the client 
                if (msg["type"] == "connect")
                {
                    // new connection
                    string clientType = msg["clientType"];
                    string clientName = "";
                    if(msg.ContainsKey("name"))
                        clientName =  msg["name"];
                    string g = Guid.NewGuid().ToString();
                    lock (_qLock)
                    {
                        WSId w = new WSId {
                            ws = ws,
                            clientType = clientType,
                            clientName = clientName
                        };
                        SocketSet[g] = w;
                    }

                    Dictionary<String, string> rd = new Dictionary<string, string>()
                    {
                        {"yourId",  g},
                        {"type", "connect"}
                    };

                    string ret = System.Text.Json.JsonSerializer.Serialize(rd);

                    // send 
                    await ws.SendStringAsync(ret, ctoken);

                    // create msg queue for this client
                    if (clientType == "pix")
                    {
                        lock (_qLock)
                        {
                            ClientQueues[g] = new Queue<Dictionary<string, string>>();
                        }
                    }
                }
                // ordinary message, all types except "connect"
                else
                {
                    // route message  "web" <-> "pix"
                    Retranslate(msg);
                }
            } else
            {
                // message does not contain field "type"
                throw new Exception("Bad msg");
            }
        }

        // ..............................................................................................

        async void Retranslate(Dictionary<string, string> msg) {
            var clientType = msg["clientType"];  // from
            var id = msg["id"];  // client from id
            // "web" -> "pix"
            if (clientType == "web")
            {
                // work with queue
                logger.log("Place to Queue");
                lock (_qLock)
                {
                    if (msg.ContainsKey("whom"))
                    {
                        // queue message to the pix client
                        ClientQueues[msg["whom"]].Enqueue(msg);
                    }
                    else
                    {
                        // broadcast to pix clients
                        foreach (var k in ClientQueues.Keys)
                        {
                            ClientQueues[k].Enqueue(msg);
                        }
                    }
                }
            }
            // "pix -> "web"
            else if (clientType == "pix")
            {
                // retranslate message to all web clients
                var clientName = SocketSet[id].clientName;

                foreach (var otherId in SocketSet.Keys)
                {
                    var otherType = SocketSet[otherId].clientType;

                    if (otherType == "web")
                    {
                        var ws = SocketSet[otherId].ws;
                        msg["name"] = clientName;
                        var toSend1 = System.Text.Json.JsonSerializer.Serialize(msg);
                        logger.log("Other state " + ws.State.ToString());

                        await ws.SendStringAsync(toSend1, ctoken);
                        logger.log($"Retrans {toSend1} from {id} to {otherId}");
                    }
                }

                string toSend;
                // if pix client queue is not empty, we send next queue message back to this pix client
                
                lock (_qLock)
                {
                    var MessageQueue = ClientQueues[id];
                    if (MessageQueue.Count > 0)
                    {
                        logger.log("Get from Queue");
                        Dictionary<string, string> msgtoSend = MessageQueue.Dequeue();
                        toSend = System.Text.Json.JsonSerializer.Serialize(msgtoSend);
                    }
                    else
                    {
                        logger.log("Queue empty, send back");
                        //mark message as echo
                        msg["echo"] = "true";
                        toSend = System.Text.Json.JsonSerializer.Serialize(msg);
                    }
                }
                await SocketSet[id].ws.SendStringAsync(toSend, ctoken);
            }
            else
            {
                // some protocol vioalation - unknown clientType
            }            
        }

        // .............................................................................

        public void Stop()
        {
            // L1
            logger.log("try to stop"); 
            // .................
            cancelTokenSource.Cancel();
            if (listener.IsListening)
            {
                    listener.Stop();
                    listener.Prefixes.Clear();
                    listener.Close();
            }
            // L4
            logger.log("try to stop OK");
        }
    }

    // .................................................................................

    [ScreenName("WSServer")]
    [Description("WSServer")]
    [Path("TL.Net")]
    [Representation("WSServer")]
    [OptionList("Start",  "Stop")]

    public class WSServer : Activity
    {
        static Server server = null;

        [ScreenName("Prefix")]
        [Description("Prefix")]
        [IsRequired]
        [Options(0)]
        public string Prefix { get; set; } = "http://localhost:3010/pix/";

        [ScreenName("Logger")]
        [Description("Logger")]
        [IsRequired]
        [Options(0)]
        public bool WriteLog { get; set; } = true;

        override public void Execute(int? opt)
        {
            switch (opt) {
                case 0:
                    if (server is null)
                    {                        
                        server = new Server(WriteLog);
                        server.Start(Prefix);
                    } else {
                        throw new Exception("Server already started");
                    }
                    break;
                case 1:
                    if (!(server is null))
                    {
                        // ?
                        server.Stop();                        
                        server = null;
                    }
                    break;
                default: break;
            }
        }
    }
}
